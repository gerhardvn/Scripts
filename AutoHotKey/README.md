# AutoHotKey

My AutoHotKey Script

* alttab with mouse wheel
* #c Run calculator
* #d Minimize all windows
* #w Maximize window over two Screens
* #a Open a command prompt in the currently selected directory in Explorer
* ^+v Strip Formatting from text in the clipboard.
* #s Spellcheck using aspell. Select any text and hit the hot key. [Install aspell](http://aspell.net/)
* #F1 To paste a list of tasks from clipboard into [Nirvana](https://nirvanahq.com/)
* #F2 To paste a list of tasks from clipboard into [Kanbanflow](https://kanbanflow.com/)
