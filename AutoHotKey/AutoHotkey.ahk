;alttab with mouse wheel
~MButton & WheelDown::AltTab
~MButton & WheelUp::ShiftAltTab
;-------------------------------------------------------------------

;calc
#c::Run calc.exe
;-------------------------------------------------------------------


; minimize all windows
#d::WinMinimizeAll
;-------------------------------------------------------------------

; maximize over two windows
;#w::
;  SysGet, mc, MonitorCount			; get monitor count
;  if (mc > 1)                         ; if there is more than one monitor
;  {
;    WinRestore, A
;    WinMove, A,, 0,0,3840, 1080
;  }
;  return
;-------------------------------------------------------------------

; command prompt here
: Open and command prompt in the currently selected directory in Explorer
#a::
WinGetText, full_path, A  ; This is required to get the full path of the file from the address bar

; Split on newline (`n)
StringSplit, word_array, full_path, `n
full_path = %word_array1%   ; Take the first element from the array

; strip to bare address
full_path := RegExReplace(full_path, "^Address: ", "")

; Just in case - remove all carriage returns (`r)
StringReplace, full_path, full_path, `r, , all  

IfInString full_path, \
{
  Run, "C:\Program Files\JPSoft\TCCLE14x64\tcc.exe"  /K cd /D "%full_path%"
}
else
{
  Run, "C:\Program Files\JPSoft\TCCLE14x64\tcc.exe"  /K cd /D "C:\ "
}
return
;-------------------------------------------------------------------

; Paste unformatted
; Strip Formatting from text in the clipboard.
^+v::
bak = %clipboard%
clipboard = %bak%
Send ^v
return
;-------------------------------------------------------------------

;Spellcheck using aspell
;Select any text and hit the hot key 
#s::
   WinGet Id, Id, A
   Send ^c
   ClipWait 2
   TmpFile = %TEMP%\SpellCheckerTmp.txt
   FileDelete %TmpFile%
   FileAppend %Clipboard%, %TmpFile%
   Runwait C:\Program Files (x86)\Aspell\bin\aspell.exe check "%TmpFile%"
   FileRead bak, %TmpFile%
   clipboard = %bak%
   WinActivate ahk_id %Id%
   WinWaitActive ahk_id %Id%
   Send ^v
Return
;-------------------------------------------------------------------

; For pasting a list of tasks into Nirvana
; got from http://autohotkey.com/board/topic/102339-copy-lines-and-paste-one-line-at-a-time/
; Simply copy a list of items, click on the rapid entry section in Nirvana and press F1
#F1::
  Loop, parse, clipboard , `r%A_Tab%
  {
    send, %A_LoopField%
    sleep, 100
  }
  Send {Enter}
return
;-------------------------------------------------------------------

#F2::
  Loop, parse, clipboard , `n, `r
  {
    send, %A_LoopField%
    sleep, 200
    send ^+{Enter}
    sleep, 2000
  }
  ;Send {Esc}
return

;Attach selected file to outlook email
#IfWinActive ahk_class CabinetWClass
#m::
  SendInput, ^c
  Sleep 100
  folderpath := clipboard 
  m := ComObjActive("Outlook.Application").CreateItem(0)
  m.Display
  m.attachments.add(folderpath)
  return
#IfWinActive  
;-------------------------------------------------------------------
