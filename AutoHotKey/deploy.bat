COPY AutoHotkey.ahk "%USERPROFILE%\My Documents\AutoHotkey.ahk"
SETLOCAL EnableExtensions
set EXE=AutoHotkey.exe
FOR /F %%x IN ('tasklist /NH /FI "IMAGENAME eq %EXE%"') DO IF %%x == %EXE% goto FOUND
echo Starting
start "C:\Program Files\AutoHotkey\AutoHotkey.exe"
goto FIN
:FOUND
echo Restarting
start /b "C:\Program Files\AutoHotkey\AutoHotkey.exe /restart"
:FIN